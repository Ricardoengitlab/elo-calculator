import React from "react";
import "./App.css";

var p = [];

for (let index = 0; index < 100; index++) {
  p.push(index / 100);
}

var d = [
  -800,
  -677,
  -589,
  -538,
  -501,
  -470,
  -444,
  -422,
  -401,
  -383,
  -366,
  -351,
  -335,
  -322,
  -309,
  -296,
  -284,
  -273,
  -262,
  -251,
  -240,
  -230,
  -220,
  -211,
  -202,
  -193,
  -184,
  -175,
  -166,
  -158,
  -149,
  -141,
  -133,
  -125,
  -117,
  -110,
  -102,
  -95,
  -87,
  -80,
  -72,
  -65,
  -57,
  -50,
  -43,
  -36,
  -29,
  -21,
  -14,
  -7,
  0,
];

for (let index = 49; index >= 0; index--) {
  d.push(d[index] * -1);
}

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      k: 0,
      ractual: 0,
      juegos: 0,
      resultado: 0,
      promedio: 0,
      total: 0,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    var tmp = this.state.ractual - this.state.promedio;
    var aux = 0;

    for (let index = 0; index < d.length; index++) {
      if (d[index] >= tmp) {
        aux = p[index];
        break;
      }
    }

    aux *= this.state.juegos;
    aux = this.state.resultado - aux;
    aux = this.state.k * aux;

    aux = this.state.ractual + aux;

    this.setState({
      // eslint-disable-next-line
      total: eval(aux),
    });
  }

  render() {
    return (
      <div className="App">
        <h1>Calculadora de rating y desempeño</h1>
        <form onSubmit={this.handleClick}>
          <label htmlFor="ka">K:</label>
          <input
            name="ka"
            type="number"
            min={0}
            onChange={(e) => this.setState({ k: e.target.value })}
            required
          />
          <label htmlFor="ractual">Rating actual: </label>
          <input
            name="ractual"
            type="number"
            min={0}
            onChange={(e) => this.setState({ ractual: e.target.value })}
            required
          />
          <label htmlFor="juegos">Número de juegos: </label>
          <input
            name="juegos"
            type="number"
            min={0}
            onChange={(e) => this.setState({ juegos: e.target.value })}
            required
          />
          <label htmlFor="resultado">Resultado del torneo: </label>
          <input
            name="resultado"
            type="number"
            step="any"
            min={0}
            onChange={(e) => this.setState({ resultado: e.target.value })}
            required
          />
          <label htmlFor="promedio">Promedio de jugadores: </label>
          <input
            name="promedio"
            type="number"
            min={0}
            onChange={(e) => this.setState({ promedio: e.target.value })}
            required
          />
          <input
            type="submit"
            value="Calcular"
            onClick={(e) => this.handleClick(e)}
            className="button"
          />
        </form>
        <h1 style={{ color: "orange" }}>Rating nuevo: {this.state.total}</h1>
      </div>
    );
  }
}

export default App;
